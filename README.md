# Game-theoretic Trajectory Planning

This repository contains all the data and code generated to obtain all the results presented in the paper "Game-theoretic trajectory planning enhances social accetability for humans"

The "Code" directory contains the code developed using Matlab R2017.

The "Data" directory contains the data collected during the experiments.
