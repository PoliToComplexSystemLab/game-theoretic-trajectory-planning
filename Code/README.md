The code used for the trajectory generation for the experiment presented in the paper "Game-theoretic trajectory planning enhances social accetability for humans" is enclosed in the "Code" folder.

The code was developed using Matlab R2017.
In the following "players" are agents (pedestrians or robot).

The logic of the algorithm (in a nutshell) is the following: 
1) First estimation of the players trajectories (assuming a constant orientation and speed of the player) in the scenario (for humans, the initial positions in the scenatio were taken from the dataset available online);
2) Solution of the model (prevision of the future trajectories of the players); 
3) Robot advancement; 
The algorithm is repeated from step 1) until the robot reaches its destination. 

The final trajectory of the robot is saved in the "intell_txt" file in which the following information is enclosed: 
1) first and second column: x,y in the map;
2) third column: frame of the video;
3) fourth column: orientation of the robot.

The files in the "Code" folder have the following functions:

1) crowds_zara01.avi: video from which we have extrapolated the simulation environment;
2) main_robot.m: file that allows to launch the simulation;
3) prime_stime.m: computes the first estimation trajectories of the players and plots the fist figure;
4) distanza.m: computes the distance between 2 points;
5) dati.m: it allows to have all the information regarding the frame of simulation (positions of players, orientaion and number of players);
6) main.m: solves the problem considering the 3 possible scenarios (1)agent does not instersect the first estimation with anyone, thus the first estimation concides with the final solution; 2)agent intersects the first estimation with someone, thus the final solution coincides with the game solution 3) agent intersects the first estimation with an obstacle, thus the final solution is the resolution of the optimization problem);
7) dati_iniziali.m: initial data (setting Hall convention, deltat, players velocity, time horizon);
8) group.m: group recognition;
9) computation_target_general.m: estimation of the player's target;
10) intersection.m: estimates which trajectory intersects the first estimate made of the players;
11) DistBetween2Segment.m: computes the distance between 2 segments;
12) obstacle.m: evalutes if from the first estimate trajectory the considered player meets the obstacles or not;
13) decel_matrix.m: generates the tree for the possibilities to decelerate;
14) calcolo_posizioni_altro_giocatore.m: computes the positions for the n-1 players;
15) obstacle2.m: useful for the tree generation to check the intersection with obstacle;
16) n_punti.m: function useful for the "decel_matrix" function;
17) cost_rall.m: computes the decelerate cost for the deceleration possibilities;
18) opt_game.m: computes the strategy of the player;
19) tree_generation.m: computes the tree of possibilities useful to compute the strategy of the player;
20) calcolo_min_dist_obstacle.m: computes the minimum distance with the intersected obstacle;
21) cost_computation.m: computes the cost useful for the tree generation;
22) best_strategy.m: computes the strategy with the minumum cost given the tree generated in "tree_generation.m";
23) opt_with_obstacle.m: computes the solution of the optimization problem considering the scenario in which the first estimation of the player intersects a obstacle;
24) tree_generation_obstacle.m; creates the tree for the "opt_with_obstacle" case;
25) intell_file.txt: output of the simulation (path of the robot with x,y,frame and orientation).
26) dati_modificati: data collected from the video.




