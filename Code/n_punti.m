function [x_n_stima,y_n_stima]=n_punti(precx,precy,aiutox,aiutoy)
m=30;

 
   if abs(precx-aiutox)>=10
       
    x_n_temp=linspace(precx,aiutox,m);
    x_n_stima=floor(x_n_temp(:));
    m_n=(aiutoy-precy)/(aiutox-precx);  
    q_n=aiutoy-m_n*aiutox;
    y_n_temp=m_n*x_n_stima+q_n;
    y_n_stima=floor(y_n_temp);
    
  else
      
    x_n_temp=precx*ones(m,1);
    x_n_stima=floor(x_n_temp);
    y_n_temp=linspace(precy,aiutoy,m);
    y_n_stima=floor(y_n_temp);
    
  end

return