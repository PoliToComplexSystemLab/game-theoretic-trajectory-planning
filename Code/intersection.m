%% output:
%giocatori_intralcianti = players who have either a very close distance between them
%close to each other (exceptions are players belonging to
%a group), or their estimated trajectories cross each other.

%x_n e y_n = useful to understand if the n-th player meets the obstacle or not 
%pos_nx e pos_ny = initial estimates of all players.

function[giocatori_intralcianti,x_n,y_n,pos_nx,pos_ny,incrocio]=intersection(n,T,giocatori,eps,gioc_x,gioc_y,group_player)

giocatori_intralcianti=[];
giocatori_intr_temp=[];

final=-10;
pos_nx=gioc_x(:,n);
pos_ny=gioc_y(:,n);
incrocio=[];
                
 
for j=1:giocatori
    
x=[];

        if j~=n
               

               if isempty(group_player)==0

                 [riga_prova]=find(j==group_player(2:end,1));

               else

                   riga_prova=[];

               end
                        
                        if isempty(riga_prova)==1
                         
                            [x_ultimo_n]=find(0~=pos_nx);
                            [y_ultimo_n]=find(0~=pos_ny);

                            [x_gioc_n]=find(0~=gioc_x(:,j));
                            [y_gioc_n]=find(0~=gioc_y(:,j));
                                                                               
                           ennesimo1=max(x_ultimo_n);
                           ennesimo2=max(y_ultimo_n);
                           
                           if ennesimo1==ennesimo2
                               
                            riga=ennesimo1(1);
                            
                           else
                               
                            temporaneo=[ennesimo1 ennesimo2];
                            riga=max(temporaneo);
                            
                           end
                                                                              
                           n2_gioc1=max(x_gioc_n);
                           n2_gioc2=max(y_gioc_n);
                            
                           if n2_gioc1==n2_gioc2
                               
                               riga2=n2_gioc1(1);
                               
                           else
                              
                               temporaneo2=[n2_gioc1 n2_gioc2];
                               riga2=max(temporaneo2);
                               
                           end
                           
                           risultato=[riga riga2];
                                                     
                           ris=min(risultato);
                                                     
                           final=ris-2; 
                           
                           
                                p1=[pos_nx(1) pos_ny(1)];
                                p2=[pos_nx(ris) pos_ny(ris)];   
                                p3=[gioc_x(1,j) gioc_y(1,j)];
                                p4=[gioc_x(ris,j) gioc_y(ris,j)];                  
                               [distance] = DistBetween2Segment(p1, p2, p3, p4); 
                               
                           
                                x_n=[pos_nx(1) pos_nx(ris)];
                                y_n=[pos_ny(1) pos_ny(ris)];
                                x_n2=[gioc_x(1,j) gioc_x(ris,j)];
                                y_n2=[gioc_y(1,j) gioc_y(ris,j)];    
                                [x]=polyxpoly(x_n,y_n,x_n2,y_n2);
                                
                        end                

         else

            distance=eps+10;

        end

                 if isempty(x)==0 || distance<eps

                     giocatori_intr_temp(end+1,1)=j;  

                 end  
      
end


%% If the players after a first estimation get in the way check if they really "get in the way" or not

if isempty(giocatori_intr_temp)==0

    dim=size(giocatori_intr_temp,1);

        for h=1:dim

            player=giocatori_intr_temp(h);
            
           if final==0
               
                      giocatori_intralcianti(end+1)=player;
                      
                      incrocio=2;
                            
           else
            
               for p=1:final

                   main_player_x=[pos_nx(p) pos_nx(p+2)];
                   main_player_y=[pos_ny(p) pos_ny(p+2)];
                   intralciante_x=[gioc_x(p,player) gioc_x(p+2,player)];
                   intralciante_y=[gioc_y(p,player) gioc_y(p+2,player)];
                   [x]=polyxpoly(main_player_x,main_player_y,intralciante_x,intralciante_y);

                   p1=[pos_nx(p) pos_ny(p)];
                   p2=[pos_nx(p+2) pos_ny(p+2)];
                   p3=[gioc_x(p,player) gioc_y(p,player)];
                   p4=[gioc_x(p+2,player) gioc_y(p+2,player)];
                   
                   d1 = distanza(p1,p3);
                   d2=distanza(p2,p4);
                   
                        
                        if isempty(x)==0 || d1 < eps || d2<eps
                            
                           
                           main_player_x=[pos_nx(p) pos_nx(p+1)];
                           main_player_y=[pos_ny(p) pos_ny(p+1)];
                           intralciante_x=[gioc_x(p,player) gioc_x(p+1,player)];
                           intralciante_y=[gioc_y(p,player) gioc_y(p+1,player)];
                           [x2]=polyxpoly(main_player_x,main_player_y,intralciante_x,intralciante_y);
                            
                           p1=[pos_nx(p) pos_ny(p)];
                           p2=[pos_nx(p+1) pos_ny(p+1)];
                           p3=[gioc_x(p,player) gioc_y(p,player)];
                           p4=[gioc_x(p+1,player) gioc_y(p+1,player)];
                          
                           d1 = distanza(p1,p3);
                           d2=distanza(p2,p4);
                            
                            if isempty(x2)==0 || d1 < eps || d2<eps
                                
                                giocatori_intralcianti(end+1)=player;
                                incrocio=p+1;
                                
                                break
                            
                            else
                                
                               giocatori_intralcianti(end+1)=player; 
                               incrocio=p+2; 
                               break 
                               
                            end

                        end
                        
               end
               
           end
           
        end

end

%% useful to check if the nth player meets the obstacle or not--> calculation of x_n and y_n which is the ouput of function 
m=50; 

   if abs(pos_nx(riga)-pos_nx(1))>=10

            x_n_temp=linspace(pos_nx(1),pos_nx(riga),m);
            x_n=floor(x_n_temp(:));
            m_n=(pos_ny(riga)-pos_ny(1))/(pos_nx(riga)-pos_nx(1));  
            q_n=pos_ny(1)-m_n*pos_nx(1);
            y_n_temp=m_n*x_n+q_n;
            y_n=floor(y_n_temp);

        else

            x_n_temp=pos_nx(1)*ones(m,1);
            x_n=floor(x_n_temp);
            y_n_temp=linspace(pos_ny(1),pos_ny(riga),m);
            y_n=floor(y_n_temp);
            
   end

return