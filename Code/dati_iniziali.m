function [n_max,T,v,eps,scelte,generazione,strategie_somma,indice,iniz,deltat]=dati_iniziali(angle,giocatori)
 


%% max number of iteration

n_max=101;


%% deltat

deltat=1.2;

%% T:Prevision time

T=4;

%% Initial velocity

v=62;

%% Hall distance

eps=45;

%% discrete set of actions

scelte=7;
strategie_somma=[0;-30;-60;-90;30;60;90];

%% generazione=generation 
generazione=4;

%% initialization of the linked index at minimum cost

indice=0;

%% initial strategy 

for i=1:giocatori
    
    strategie=ones(T+1,1);
   strategie(1)=angle(i);
    %strategie=angle(i)*ones(T+1,1)
    iniz(:,i)=strategie(:);

end



return