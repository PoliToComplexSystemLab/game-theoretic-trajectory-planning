
function  [strategie_finali_k,posizioni_finali_x,posizioni_finali_y,nodes,value]=opt_game(riga_shrinking,Robot_x_target,Robot_y_target,xStart,yStart,strategie_finali_k,deltat,v,T,angle,n,scelte,strategie_somma,eps,X,Y,MAP,giocatori,starx,stary,ostacolo_present,Ostacolo_x,Ostacolo_y,incrocio_ostacolo,generazioni_da_considerare,group_player,posizioni_finali_x,posizioni_finali_y,f)

    if f==1 & n==1

        [posx,posy]=calcolo_posizioni_altro_giocatore(xStart,yStart,strategie_finali_k,n,deltat,v,T,giocatori); 

    else

         posx=posizioni_finali_x;
         posy=posizioni_finali_y;

    end
    
   
    if n==giocatori
        
        targetx_n=Robot_x_target;
        targety_n=Robot_y_target;
        
    else
        
          targetx_n=starx(n);
          targety_n=stary(n);
      
    end
    
    %% tree generation 

    [nodes]=tree_generation(riga_shrinking,xStart,yStart,angle,n,scelte,strategie_somma,deltat,v,posx,posy,eps,targetx_n,targety_n,X,Y,MAP,ostacolo_present,Ostacolo_x,Ostacolo_y,incrocio_ostacolo,generazioni_da_considerare,group_player);

    %% computation of the best strategy 

    [idx,value]=best_strategy(nodes,riga_shrinking,scelte);

    %% update the strategy 
   
    strategie_finali_k(:,n)=zeros(T+1,1);
    strategie_finali_k(1:riga_shrinking,n)=nodes(:,1,idx,riga_shrinking);
       
    posizioni_finali_x(:,n)=zeros(T+1,1);
    posizioni_finali_y(:,n)=zeros(T+1,1);
    posizioni_finali_x(1:riga_shrinking,n)=nodes(:,4,idx,riga_shrinking);
    posizioni_finali_y(1:riga_shrinking,n)=nodes(:,5,idx,riga_shrinking);

                                
return