function [posx,posy]=calcolo_posizioni_altro_giocatore(xStart,yStart,strategie_finali_k,n,deltat,v,T,giocatori)

%initialization
posx=zeros(T+1,giocatori);
posy=zeros(T+1,giocatori);

    for j=1:giocatori

        if j~=n   

            px=xStart(j);
            py=yStart(j);
            posx(1,j)=px;
            posy(1,j)=py;

                for i=2:T+1

                    px=px+v*cosd(strategie_finali_k(i,j))*deltat;
                    py=py+v*sind(strategie_finali_k(i,j))*deltat;

                   
                    posx(i,j)=px;
                    posy(i,j)=py;

                end

        end 

    end 

return
   
   