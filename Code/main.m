%% descrizioni matrici:

% nodes=matrice a 4 dimensioni--> nodes(tempi,colonne,figli,generazione)-->
%nodes(colonne)=strategie,vincolo rispettato=1 non rispettato=0,costo,coordinatex,coordinatey)
% pos=posizioni dei giocatori intralcinati.  
% MAP--> 0=occupato,1=libero
% angle: contiene la prima strategia osservata dal robot al p-esimo passo 
%occhio che le strategie_finali che stampano sono le strategie che vengono
%azzerate dopo il calcolo dell'equilibrio di Nash


%starx, stary= target stimato per i pedoni 

%% acquisizione immagine 
function[deltat,riga_shrinking,starx,stary,posizioni_finali_x,posizioni_finali_y,riga_temp,angle_save]=main(contatore,xStart,yStart,MAP,angle,giocatori,Robot_x_target,Robot_y_target,Ostacolo_x,Ostacolo_y,angle_save)


BW=MAP;
 

  X=size(MAP,2);
  Y=size(MAP,1);
  xStart_iniz=xStart;
  yStart_iniz=yStart;
  angle_iniz=angle;
  
   
 
[n_max,T,v,eps,scelte,generazione,strategie_somma,indice,iniz,deltat]=dati_iniziali(angle,giocatori);

%% group recognition  

[gruppo]=group(angle,xStart,yStart,giocatori);


%% initialization  
nodes1={};
strategie_finali_k_1=iniz;
strategie_finali_k=iniz;
giocatori_ciclo={};
posizioni_finali_x=zeros(T+1,giocatori);
posizioni_finali_y=zeros(T+1,giocatori);
save_nx=zeros(T,giocatori);
save_ny=zeros(T,giocatori);
starx=zeros(giocatori,1);
stary=zeros(giocatori,1);
salvo_gen_da_cons={};
computation_dist=zeros(T,1);
riga_prova=[];
group_player=[];

  %% game creation considering a receding horizon 
 
  p=contatore-1;
    
%% useful parameter to exit internal cycles and understand if you have reached Nash equilibrium

    esci=2;
    
          for f=1:n_max
              
              % "exit" usefull to understand if the robot is reached the
              % target or not 
              
                   exit=0;
                   
                   for n=1:giocatori
                       
                       if isempty(gruppo)==0
                                                      
                             [riga_prova1,colonna]=find(n==gruppo);
                             
                             [righe]=find(gruppo(:,colonna)~=0);
                             group_player=gruppo(righe,colonna);
                             
                             if isempty(group_player)==0
                                 
                                [riga_prova]=find(n==group_player(2:end,1));   
                                
                             end
                       else
                          
                           riga_prova=[];
                        
                       end
                       
                       if isempty(riga_prova)==1 | isempty(group_player)==1
                       
                         %% target estimation for all players 
                         
                        if f==1 && n==1
                            
                            for l=1:giocatori
                                
                                if l==giocatori
                                  
                                    starx(l)=Robot_x_target;
                                    stary(l)=Robot_y_target;
                                    
                                else
                                
                                    [starx_n2,stary_n2,angle_save]=computation_target_general(v,angle,T,xStart,yStart,l,p,angle_save,deltat);  
                                    starx(l)=starx_n2;
                                    stary(l)=stary_n2;
                                    
                                end
                                
                            end
                            
                             %% plot figure 1 and first estimation of the player
     
                            [gioc_x,gioc_y,riga_temp]=prime_stime(p,giocatori,xStart,yStart,T,v,angle,deltat,BW,Robot_x_target,Robot_y_target,starx,stary,angle_save);
                                
                        end
                        
                         if n==giocatori
                              
                               angle_gioc=atan2(stary(n)-yStart(n),starx(n)-xStart(n));
                               
                               gradi=rad2deg(angle_gioc);
                              
                               angle(giocatori)=gradi;
                               angle_save(p,giocatori)=gradi;
                               
                               strategie_finali_k(1,n)=gradi;
                               
                               riga_shrinking=riga_temp;
                                  
                          else

                              riga_shrinking=T+1;

                         end
                          
                  %% "intersection" find the trajectories that intersect at the n-th player
                          
                         [giocatori_intralcianti,x_n,y_n,pos_nx,pos_ny,incrocio]=intersection(n,T,giocatori,eps,gioc_x,gioc_y,group_player);
                         giocatori_ciclo{n}=giocatori_intralcianti;
                         
                         %% "obstacle" evalutes if from the first estimate the considered players meets the obstacles or not
                        
                        [obst,incrocio_ostacolo,generazioni_da_considerare]=obstacle(x_n,y_n,MAP,X,Y,Ostacolo_x,Ostacolo_y,pos_nx,pos_ny,T);
                        salvo_gen_da_cons{n}=generazioni_da_considerare;
   
                  
                      
                         generazioni_da_considerare=salvo_gen_da_cons{n};
                         
                          
                               
                           p1=[starx(giocatori) stary(giocatori)];
                           p2=[xStart(giocatori) yStart(giocatori)];
                           prova_dist=distanza(p1,p2);

                           if n==giocatori & prova_dist<=80

                               posizioni_finali_x(:,giocatori)=zeros(T+1,1);
                               posizioni_finali_y(:,giocatori)=zeros(T+1,1);
                               posizioni_finali_x(1,giocatori)=xStart(giocatori);
                               posizioni_finali_y(1,giocatori)=yStart(giocatori); 
                               riga_temp=1;
                               exit=1;

                           end
                           
                           
                           
                      
                        %% if player n has no hindering players and if he does not hit any obstacle then his strategy is to continue on his way (also applies to the robot)
                          
                       
                           if isempty(giocatori_intralcianti)==1 & obst==0 & f==1 & exit==0
                               
                                  angle_gioc=atan2(stary(n)-yStart(n),starx(n)-xStart(n));
                                  gradi=rad2deg(angle_gioc);
                                  
                                  strategie_finali_k(:,n)=gradi*ones(T+1,1);  
                                  
                                
                                  
                                  posizioni_finali_x(:,n)=zeros(T+1,1);
                                  posizioni_finali_y(:,n)=zeros(T+1,1);
                                  posizioni_finali_x(1,n)=xStart(n);
                                  posizioni_finali_y(1,n)=yStart(n);
                                  
                                  for i=1:(riga_shrinking-1)
                                      
                                      posizioni_finali_x(i+1,n)=posizioni_finali_x(i,n)+v*cosd(gradi)*deltat;
                                      posizioni_finali_y(i+1,n)=posizioni_finali_y(i,n)+v*sind(gradi)*deltat;
                                      
                                      if n==giocatori & riga_shrinking~=T+1
                                          
                                          
                                          eps1=[0.6;0.7;0.8;1];
                                          p1=[posizioni_finali_x(i+1,n) posizioni_finali_y(i+1,n)];
                                          p2=[starx(n) stary(n)];
                                          dist_cost=distanza(p1,p2);
                                          costo(i)=eps1(i)*dist_cost;
                                         
                                      end
                                      
                                  end
                                  
                                 %evaluate the possibility to slow down in the
                                 % case of the robot with shrinking horizon:
                                  
                                  if n==giocatori & riga_shrinking~=T+1
                                      
                                    incrocio=riga_shrinking;   
                                    
                                     [pos_ral_x,pos_ral_y,strategia,costo_rall]=decel_matrix(riga_shrinking,incrocio,v,angle_save,p,giocatori,n,xStart,yStart,deltat,strategie_finali_k,eps,starx,stary,MAP,X,Y,f,posizioni_finali_x,posizioni_finali_y,Robot_x_target,Robot_y_target,T);
                                     
                                     %computes the minumum cost between the
                                     %2 possibilities (cost_rall and costo_cost)
                                     
                                     costo_cost=sum(costo);
                                     
                                    if costo_rall<costo_cost
                                        
                                        posizioni_finali_x(:,n)=pos_ral_x;
                                        posizioni_finali_y(:,n)=pos_ral_y;
                                        
                                    end
                                      
                                  end
                                  
                                

                           end  
                        
                       
                        %% Strategy calculation if the player of interest itersects the other players (also applied for the robot)
                        
                       if (isempty(giocatori_intralcianti)==0 & obst==1 & exit==0) || (isempty(giocatori_intralcianti)==0 & obst==0 & exit==0) 
                           
                           
                           ostacolo_present=obst;
                           [strategie_finali_temp,pos_temp_x,pos_temp_y,nodes,value]=opt_game(riga_shrinking,Robot_x_target,Robot_y_target,xStart,yStart,strategie_finali_k,deltat,v,T,angle,n,scelte,strategie_somma,eps,X,Y,MAP,giocatori,starx,stary,ostacolo_present,Ostacolo_x,Ostacolo_y,incrocio_ostacolo,generazioni_da_considerare,group_player,posizioni_finali_x,posizioni_finali_y,f);
                           nodes1{n}=nodes;
                           
                           %% function that tries to calculate the possibility of slowing down considering a constant direction
                          
                           [pos_ral_x,pos_ral_y,strategia,costo_finale]=decel_matrix(riga_shrinking,incrocio,v,angle_save,p,giocatori,n,xStart,yStart,deltat,strategie_finali_k,eps,starx,stary,MAP,X,Y,f,posizioni_finali_x,posizioni_finali_y,Robot_x_target,Robot_y_target,T);  
                           
                           
                           if value > costo_finale 
                           
                                   
                               posizioni_finali_x(:,n)=pos_ral_x;
                               posizioni_finali_y(:,n)=pos_ral_y;
                               strategie_finali_k(:,n)=strategia;

                           else 
                               
                               posizioni_finali_x=pos_temp_x;
                               posizioni_finali_y=pos_temp_y;
                               strategie_finali_k=strategie_finali_temp;
                                
                           end
                           
                     end
                       
                       %% case in which the player of interest has no hindering players but must avoid an obstacle 
                             
                        
                         if isempty(giocatori_intralcianti)==1 & obst==1 & f<=2 & exit==0 

                          riga_temp=T+1;
                          riga_shrinking=riga_temp;
                          [strategie_finali_k,posizioni_finali_x,posizioni_finali_y,value,nodes]=opt_with_obstacle(riga_shrinking,Robot_x_target,Robot_y_target,v,angle,T,xStart,yStart,n,scelte,strategie_somma,deltat,generazione,X,Y,MAP,posizioni_finali_x,posizioni_finali_y,strategie_finali_k,Ostacolo_x,Ostacolo_y,incrocio_ostacolo,generazioni_da_considerare,starx,stary,giocatori,eps);
                          aiutooo{p}=nodes;
                          
                         
                     
                               if n==giocatori

                                  for h=2:T+1

                                      p1=[posizioni_finali_x(h,giocatori) posizioni_finali_y(h,giocatori)];
                                      p2=[Robot_x_target Robot_y_target];
                                      dist_robot_target=distanza(p1,p2);
                                      computation_dist(h-1,1)=dist_robot_target;

                                  end

                                 [d1,position]=min(computation_dist);

                                     if d1<=100
                                         
                                          if position<T
                                              
                                              riga_temp=position+1;
                                              riga_shrinking=riga_temp;
                                              [strategie_finali_k,posizioni_finali_x,posizioni_finali_y,value,nodes]=opt_with_obstacle(riga_shrinking,Robot_x_target,Robot_y_target,v,angle,T,xStart,yStart,n,scelte,strategie_somma,deltat,generazione,X,Y,MAP,posizioni_finali_x,posizioni_finali_y,strategie_finali_k,Ostacolo_x,Ostacolo_y,incrocio_ostacolo,generazioni_da_considerare,starx,stary,giocatori,eps);
                                              aiutooo2{p}=nodes;
                                              
                                          end
                                          
                                          incrocio=riga_shrinking;
                                          [pos_ral_x,pos_ral_y,strategia,costo_obst]=decel_matrix(riga_shrinking,incrocio,v,angle_save,p,giocatori,n,xStart,yStart,deltat,strategie_finali_k,eps,starx,stary,MAP,X,Y,f,posizioni_finali_x,posizioni_finali_y,Robot_x_target,Robot_y_target,T);

                                          if costo_obst<value

                                            posizioni_finali_x(:,giocatori)=pos_ral_x;
                                            posizioni_finali_y(:,giocatori)=pos_ral_y;

                                          end

                                     end


                               end
                              
                         end
                      
                      else
                          %% Extension to the whole group of what was previously calculated
                           
                          strategie_finali_k(:,n)=strategie_finali_k(:,group_player(1,1));
                          posizioni_finali_x(1,n)=xStart(n);
                          posizioni_finali_y(1,n)=yStart(n);
                          
                          for indice=1:T
                              
                             posizioni_finali_x(indice+1,n)=posizioni_finali_x(indice,n)+deltat*v*cosd(strategie_finali_k(indice+1,n));
                             posizioni_finali_y(indice+1,n)=posizioni_finali_y(indice,n)+deltat*v*sind(strategie_finali_k(indice+1,n));
                            colonne=floor(posizioni_finali_x(indice+1,n));
                            righe=floor(posizioni_finali_y(indice+1,n));
                            
                                                   
                         end
                           
                      end   
                      
                       %% Nash equilibrium--> calculated after n--> full cycle (first cycle done safely with f==2)

                                 matrice_controllo=5*ones(T+1,giocatori);
                                 
                                 if norm(strategie_finali_k-strategie_finali_k_1)<=matrice_controllo & f>=2
                                    
                                    
                                     temp=strategie_finali_k(2,:);
                                     strategie_finali_k_1=ones(T+1,giocatori);
                                     strategie_finali_k_1(1,:)=temp;
                                    
                                    
                                    
                                    strategie_finali_k=strategie_finali_k_1;
                                    esci=1;
                                   
                                    break
                                    
                                 else

                                    strategie_finali_k_1=strategie_finali_k(:,:);
                                 
                                 end
                       
                  end

                  if esci==1 
                        
                      break
                      
                  end

          end
         
 return

