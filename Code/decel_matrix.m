%the function computes the tree for the possibilies to decelerate;


function [pos_ral_x,pos_ral_y,strategia,costo_finale]=decel_matrix(riga_shrinking,incrocio,v,angle_save,p,giocatori,n,xStart,yStart,deltat,strategie_finali_k,eps,starx,stary,MAP,X,Y,f,posizioni_finali_x,posizioni_finali_y,Robot_x_target,Robot_y_target,T)

 matrice=zeros(riga_shrinking,5,(2^2-1));
 costo=zeros(1,3);
 intersection_control=zeros(giocatori,1);
 pos_ral_x(1,:)=xStart(n)*ones(1,3);
 pos_ral_y(1,:)=yStart(n)*ones(1,3);
 
 if f==1 & n==1
     
    [posx,posy]=calcolo_posizioni_altro_giocatore(xStart,yStart,strategie_finali_k,n,deltat,v,T,giocatori);
    
 else
     
     posx=posizioni_finali_x;
     posy=posizioni_finali_y;
     
 end
 
  decel=[0.5*v 0.6*v 0.7*v 0.9*v];
  dim_decel=size(decel,2);
  
for k=1:dim_decel
 
     v_possibility=[v v-decel(k);
                    v-decel(k) v;
                    v-decel(k) v-decel(k)];
     v_new=v*ones(T+1,1); 
     
    
     for i=1:(3) 
         
           matrice(:,1,i)=angle_save(p,n)*ones(riga_shrinking,1);
           
           v_new(incrocio-1,1)=v_possibility(i,1);
           v_new(incrocio,1)=v_possibility(i,2);
           
           matrice(1,3,i)= xStart(n);
           matrice(1,4,i)=yStart(n);
           
           for j=1:riga_shrinking-1
               
                pos_ral_x(j+1,i)=pos_ral_x(j,i)+deltat*v_new(j+1,1)*cosd(angle_save(p,n));
                pos_ral_y(j+1,i)=pos_ral_y(j,i)+deltat*v_new(j+1,1)*sind(angle_save(p,n));
                matrice(j+1,3,i)=pos_ral_x(j+1,i);
                matrice(j+1,4,i)=pos_ral_y(j+1,i);
                ennesimo=[pos_ral_x(j+1,i) pos_ral_y(j+1,i)];
                
                [x_n_stima,y_n_stima]=n_punti(pos_ral_x(j,i),pos_ral_y(j,i),pos_ral_x(j+1,i),pos_ral_y(j+1,i));
                [obst]=obstacle2(x_n_stima,y_n_stima,MAP,X,Y);

                
                matrice(1,2,i)=1;
                intersection_control=zeros(giocatori,1);
                
                for l=1:giocatori
                    
                    if l~=n 

                        altro_giocatore=[posx(j+1,l) posy(j+1,l)];
                        d(l,1)=distanza(ennesimo,altro_giocatore);
                        
                        px_n=[pos_ral_x(j,i) pos_ral_x(j+1,i)];
                        py_n=[pos_ral_y(j,i) pos_ral_y(j+1,i)];
                        px_n2=[posx(j,l) posx(j+1,l)];
                        py_n2=[posy(j,l) posy(j+1,l)];
                        [x]=polyxpoly(px_n,py_n,px_n2,py_n2);
                        
                        if isempty(x)==0
                            
                            intersection_control(l,1)=x;

                          
                            
                        end
                        
                    else
            
                        d(l,1)=eps+10;

                    end

                end
                
                if d>eps*ones(giocatori,1) & obst==0 & intersection_control==zeros(giocatori,1)

                  matrice(j+1,2,i)=1;

                end
            
           end
           
                      
           if n==giocatori
               
              targetx_n = Robot_x_target;
              targety_n =Robot_y_target;
              
           else
               
              targetx_n=starx(n);
              targety_n=stary(n);
              
           end
           
        % if at all time instants the constraints are respected then
        
        if matrice(:,2,i)==ones(riga_shrinking,1)
                                  
            val=50;
            ro=1;
            
                for z=1:(riga_shrinking-1)

                    [cost]=cost_rall(targetx_n,targety_n,matrice,z,val,ro,i);
                    costo_temp(1,z)=cost;

                end
                
            costo(1,i)=sum(costo_temp);
            
        else
              
            costo(1,i)=inf;
           
        end
        
    end
    
    [c,colonna]=min(costo);
    costo_temporaneo(k)=c;
    pos_temp_x(:,k)=matrice(:,3,colonna);
    pos_temp_y(:,k)=matrice(:,4,colonna);
  
end  

    [c_fin,colonna_k]=min(costo_temporaneo);
    costo_finale=c_fin; 
    
    strategia=zeros(T+1,1);
    strategia(1:riga_shrinking,1)=matrice(:,1,colonna);
    
    pos_ral_x=zeros(T+1,1);
    pos_ral_y=zeros(T+1,1);
    
    pos_ral_x(1:riga_shrinking,1)=pos_temp_x(:,colonna_k);
    pos_ral_y(1:riga_shrinking,1)=pos_temp_y(:,colonna_k);
    
return





