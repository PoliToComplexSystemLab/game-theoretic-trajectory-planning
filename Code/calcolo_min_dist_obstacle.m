function[val]=calcolo_min_dist_obstacle(xpos,ypos,incrocio_ostacolo,Ostacolo_x,Ostacolo_y)

pos_pedone=[xpos,ypos]; 
     f=[-2*xpos; -2*ypos];
     H=[2 0;0 2];
     lb=[Ostacolo_x(1,incrocio_ostacolo);Ostacolo_y(1,incrocio_ostacolo)];
     ub=[Ostacolo_x(2,incrocio_ostacolo);Ostacolo_y(2,incrocio_ostacolo)];
     A=[];
     b=[];
     Aeq=[];
     beq=[];
    sol=quadprog(H,f,A,b,Aeq,beq,lb,ub);
     [dis]=distanza(sol,pos_pedone);
    val=dis;    
 
return