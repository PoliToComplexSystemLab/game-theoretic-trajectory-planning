%estimation of the target for the "giocatore"
function [starx,stary,angle_save]=computation_target_general(v,angle,T,xStart,yStart,giocatore,p,angle_save,deltat)
            
            
            alfa=ones(p,1);
            alfa(1,1)=1;
            alfa(2:p,1)=0.85;
                       
            starx=xStart(giocatore)+v*T*deltat*cosd(angle(giocatore));
            stary=yStart(giocatore)+v*T*deltat*sind(angle(giocatore));   
            starx=floor(starx);
            stary=floor(stary);
            
           
            if p>1 & abs(angle_save(p-1,giocatore)-angle(giocatore))<140 & angle_save(p-1,giocatore)~=361

                angle_save(p,giocatore)=(1-alfa(p))*angle_save(p-1,giocatore)+alfa(p)*angle_save(p,giocatore); 
                starx=xStart(giocatore)+v*T*deltat*cosd(angle_save(p,giocatore));
                stary=yStart(giocatore)+v*T*deltat*sind(angle_save(p,giocatore)); 
                
            end
            
            if (p>1 & abs(angle_save(p-1,giocatore)-angle(giocatore))>=140 & abs(angle_save(p-1,giocatore)-angle(giocatore))<=360) | (p>1 & angle_save(p-1,giocatore)==361) 
       
                angle_save(p,giocatore)=angle(giocatore);
                starx=xStart(giocatore)+v*T*deltat*cosd(angle_save(p,giocatore));
                stary=yStart(giocatore)+v*T*deltat*sind(angle_save(p,giocatore)); 
                
            end
return              