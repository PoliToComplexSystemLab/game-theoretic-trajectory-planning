clear all
close all
clc

%%pos_x e pos_y = are the robot's positions every deltat  
%%deltat = 1.2s
%%updating and recalculation of the robot positions every 0.5s--> i.e. every
%%12.5 frames, considering that the video was built with 25 frames per second. 

%data collected from the environment
load dati_modificati

%simulation environment 
Zara=VideoReader('crowds_zara01.avi');
max_frame=Zara.NumberOfFrames;
T=4;

%% Designed of the black and white map, 0:occupied, 1:free
MAP=ones(576,720);

MAP(1:243,70:444)=zeros(243,375);
MAP(1:303,445:560)=zeros(303,116);
MAP(492:563,65:297)=zeros(72,233);
X=size(MAP,2);
Y=size(MAP,1);
MAP1=MAP;

 %% Obstacles
  
Ostacolo_x=[70 445 65;
     444 560 297];

 Ostacolo_y=[0 0 492;
     243 303 563]; 
  
%% set the frame in which the robot will start to move (frame), initial (x_robot, y_robot) and final poses of the robot (x_robot_target, y_robot_target)


frame=3200;

%start

x_robot=0;
y_robot=500;


% target robot

x_robot_target=700;
y_robot_target=150;


p1=[x_robot y_robot];
p2=[x_robot_target y_robot_target];
d2=distanza(p1,p2);

pos_x(1,1)=x_robot;
pos_y(1,1)=y_robot;
frame_vector(1,1)=frame;

%counter useful to save every deltat the position, the frame and the
%orientation of the robot
contatore=2;

% for active until the robot arrives at its destination 

for i=1:max_frame
        
    %players' position and orientation in the frame environment 

     [xStart,yStart,giocatori,angle]=dati(frame,Zara,crowdszara01,max_frame);
     
     % among the players I also consider the robot 
     
      giocatori=giocatori+1;
          
    %Information about robot
    
     xStart(giocatori)=x_robot;
     yStart(giocatori)=y_robot;
    radianti =atan2(y_robot_target-y_robot,x_robot_target-x_robot);
    trasformo=rad2deg(radianti);
    angle(giocatori)=trasformo;  
    
     if contatore==2
         
        angle_save(1,:)=angle;
        
     else
         
         %transformation useful because after each deltat the pedestrians
         %in the scene change
         
         angle_save_temp=angle_save;
         dim_previous_player=size(angle_save,2);
         
         if dim_previous_player>giocatori
             
              angle_save_temp2=361*ones(contatore-1,dim_previous_player);
              angle_save_temp2(1:contatore-2,1:dim_previous_player)=angle_save;
              angle_save_temp2(contatore-1,1:giocatori)=angle(1:giocatori);
              angle_save=angle_save_temp2; 
        
              
         else
             
             angle_save_temp2=361*ones(contatore-1,giocatori);
             
             %% if the players increase insert the robot in the last place anyway 
             
             angle_save_temp2(1:contatore-2,1:dim_previous_player-1)=angle_save(:,1:dim_previous_player-1);
             angle_save_temp2(1:contatore-2,giocatori)=angle_save(:,dim_previous_player);

             angle_save_temp2(contatore-1,1:giocatori)=angle(1:giocatori);
             angle_save=angle_save_temp2; 
        
  
         end
        
         
     end
     
     %% Solution of the scenario 
   
[deltat,riga_shrinking,starx,stary,posizioni_finali_x,posizioni_finali_y,riga_temp,angle_save]=main(contatore,xStart,yStart,MAP,angle,giocatori,x_robot_target,y_robot_target,Ostacolo_x,Ostacolo_y,angle_save);
     

 %% Plot the model solution 
 
    figure(2)
    clf 
   b = read(Zara,frame+1);
    imshow(b);
    title('Model solution')
    hold on
    axis on 
    hold on

    for i=1:giocatori

           if i==giocatori

              riga_shrinking=riga_temp;
              
              plot(xStart(i),yStart(i),'xr');
              text(xStart(i)-20,yStart(i)+10,'Start robot');

              hold on 

             plot(starx(i),stary(i),'or');
             text(starx(i)-20,stary(i)-10,'Target robot');

          else

              riga_shrinking=T+1;
              
               plot(xStart(i),yStart(i),'xr');
               text(xStart(i)-20,yStart(i)+10,['Start',num2str(i)]);
               hold on 
               plot(starx(i),stary(i),'or');
               text(starx(i)-20,stary(i)+10,['Target',num2str(i)]);

           end      

              hold on                                                               
              plot(posizioni_finali_x(1:riga_shrinking,i),posizioni_finali_y(1:riga_shrinking,i),'o');
              hold on
            
            if riga_shrinking~=1

              PathPoints = [interp1(1:riga_shrinking,posizioni_finali_x(1:riga_shrinking,i),linspace(1,riga_shrinking,101)','PCHIP','extrap') interp1(1:riga_shrinking,posizioni_finali_y(1:riga_shrinking,i),linspace(1,riga_shrinking,101)','PCHIP','extrap')];
              plot(PathPoints(:,1),PathPoints(:,2));

            end

    
       % pause
            
    end
    
%pause % to see the plots before the next time instant is calculated 
      

    if posizioni_finali_y(2,giocatori)==0 & posizioni_finali_x(2,giocatori)==0

        %ituation in which the simulator cannot find a solution  I assume that it stays still 
          x_robot=x_robot+2;
          y_robot=y_robot+2;
          
    else
        deltat_riagg=0.5;
        radianti_prova =atan2(posizioni_finali_y(2,giocatori)-y_robot,posizioni_finali_x(2,giocatori)-x_robot);
        trasformazione=rad2deg(radianti_prova);
        distanza_2 = sqrt((posizioni_finali_y(2,giocatori)-y_robot)^2 + (posizioni_finali_x(2,giocatori)-x_robot)^2);
        v_computed= distanza_2 /deltat;
        x_robot=x_robot+v_computed*cosd(trasformazione)*deltat_riagg;
        y_robot=y_robot+v_computed*sind(trasformazione)*deltat_riagg;
    end

%vector that saves what will be plotted on the video
pos_x(contatore,1)=x_robot;
pos_y(contatore,1)=y_robot;
   
  
 %calculation of the distance of the robot from time t to the target
 p1=[x_robot y_robot];
 p2=[x_robot_target y_robot_target];
 d2=distanza(p1,p2);
 
 
 frame=frame+13;
 frame_vector(contatore,1)=frame;
 contatore=contatore+1;
 

    %check if the robot has arrived --> then exit the external for
    
     if d2<=70 | (x_robot==0 & y_robot==0)

        break

     end
 
end
 
if pos_x(end,1)==0 & pos_y(end,1)==0
    
    pos_x(end)=x_robot_target;
    pos_y(end)=y_robot_target;
    frame=frame+13;  
    frame_vector(end,1)=frame;
       
else
    
    pos_x(end+1)=x_robot_target;
    pos_y(end+1)=y_robot_target;
    
   
    frame=frame+13;        
    frame_vector(end+1,1)=frame;
    
end

% trasformo le posizioni del robot dal ref frame della mappa al ref frame
% del video

%% conversion from the validation reference system to the reference of the video and plot sw

[valore_min_x]=min(crowdszara1(:,1));
[valore_max_y]=max(pos_y(:,1));
[valore_min_y]=min(crowdszara1(:,2));
[max_y_primo]=max(crowdszara1(:,2));
dim=size(pos_x,1);

pos_x_video=pos_x(:,1)+valore_min_x*ones(dim,1);
x_target_video=x_robot_target+valore_min_x;


temp_y_video=pos_y(:,1)-(max_y_primo+50)*ones(dim,1);
pos_y_video=-temp_y_video;


y_target_video_temp=y_robot_target-(max_y_primo+50);
y_target_video=-y_target_video_temp;

%% gaze direction video
% change the reference system of the video by rotating it 90 degrees
%with respect to the z axis
% p''=R(-90�,z')*p'

pos_x_video_cambio=pos_y_video;
pos_y_video_cambio=-pos_x_video;
x_target_video_cambio=y_target_video;
y_target_video_cambio=-x_target_video;
gaze_direction(1,1)=atan2(y_target_video_cambio-pos_y_video_cambio(1,1),x_target_video_cambio-pos_x_video_cambio(1,1));

for i=1:dim-1
    
    gaze_direction(i+1,1)=atan2(pos_y_video_cambio(i+1,1)-pos_y_video_cambio(i,1),pos_x_video_cambio(i+1,1)-pos_x_video_cambio(i,1));
    
end

gaze_direction_gradi=rad2deg(gaze_direction);
matrice(:,1)=pos_x_video;
matrice(:,2)=pos_y_video;
matrice(:,3)=frame_vector;
matrice(:,4)=gaze_direction_gradi;

%% write the plot file 

dim_matrice=size(matrice,1);

fileID = fopen('intell_file.txt','w');

fprintf(fileID,'%d %12s\n',dim_matrice,'- Num of control points');

%% Print the solution at 1 Hz even though the calculation was done at 2Hz. 
% This choice is due to the fact that the software that reads the file can not support such a high frequency.

for i=1:2:dim_matrice
    
fprintf(fileID,'%6.2f %12.8f %d %6.2f %12s\n',matrice(i,:),'- (2D point, m_id)');

end
fclose(fileID);
