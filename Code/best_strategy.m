%% goal:compute the path with the minimum cost
 
function [idx,value]=best_strategy(nodes,riga_shrinking,scelte)

            scelta=nodes(:,:,:,riga_shrinking);
            figlio_fin=scelte^(riga_shrinking-1);
            vet_finale=zeros(1,figlio_fin);
            
            for i=1:figlio_fin 
                    controllo_vincolo=scelta(:,2,i);
                    
                    if  controllo_vincolo==ones(riga_shrinking,1)

                       costo_finale=sum(scelta(:,3,i));
                       vet_finale(i)=costo_finale;
                       
                    else

                       vet_finale(i)=inf; 

                    end
            
            end
            
         [value,idx]=min(vet_finale);

return