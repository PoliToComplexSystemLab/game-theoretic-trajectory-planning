function [nodes]=tree_generation_obstacle(riga_shrinking,eps,xStart,yStart,angle,n,T,scelte,strategie_somma,deltat,v,generazione,targetx_n,targety_n,X,Y,MAP,Ostacolo_x,Ostacolo_y,incrocio_ostacolo,generazioni_da_considerare,posx,posy,giocatori)


%% inizialization of the first generation
        
            angle_prima_gen=angle(n);
            nodes=zeros(riga_shrinking,5,1,1);
            nodes(1,1,1,1)=angle_prima_gen;
            nodes(1,2,1,1)=1;
            nodes(1,3,1,1)=0;
            nodes(1,4,1,1)=xStart(n); 
            nodes(1,5,1,1)=yStart(n);
           
 %% creation of the tree for the receeding horizon 
 
for i=1:(riga_shrinking-1)
    
    figli=scelte^(i-1);
   
    m=1;
        for j=1:figli
            
             salvo=zeros(riga_shrinking,5,scelte,1);

                for z=1:scelte 
                    
                                
                      salvo(:,:,z,1)=nodes(:,:,j,i);
                      gradi=strategie_somma(z); 
                      
                     
                        if salvo(1:i,2,z,1)==ones(i,1)
                            
                                salvo(i+1,1,z,1)=salvo(i,1,z,1)+gradi;
                                nodes(:,:,m,i+1)= salvo(:,:,z,1);
                                
                               

                                posizione_x=nodes(i,4,m,i+1)+v*deltat*cosd(nodes(i+1,1,m,i+1));
                                nodes(i+1,4,m,i+1)=floor(posizione_x);
                                posizione_y=nodes(i,5,m,i+1)+v*deltat*sind(nodes(i+1,1,m,i+1));
                                nodes(i+1,5,m,i+1)=floor(posizione_y);
                                
                                

                                controllo=[nodes(i+1,4,m,i+1) nodes(i+1,5,m,i+1)];
                                precx=floor(nodes(i,4,m,i+1));
                                precy=floor(nodes(i,5,m,i+1));
                                aiutox=floor(controllo(1));
                                aiutoy=floor(controllo(2));
                                
                                
                                    
                                   [x_n_stima,y_n_stima]=n_punti(precx,precy,aiutox,aiutoy);
                                   [obst]=obstacle2(x_n_stima,y_n_stima,MAP,X,Y);
                                   
                              
                              
                                dim=size(posx,2); 
                                intersection_control=zeros(dim,1);
                                
                                if giocatori~=1
                                    
                                    for l=1:dim

                                        if l~=n

                                           controllo2=[posx(i+1,l) posy(i+1,l)];
                                           d(l,1)=distanza(controllo,controllo2);
                                           
                                               if i+1<3 
                                                   ped_av_n_x=[nodes(i,4,m,i+1) nodes(i+1,4,m,i+1)];
                                                   ped_av_n_y=[nodes(i,5,m,i+1) nodes(i+1,5,m,i+1)];
                                                   ped_av_n2_x=[posx(i,l) posx(i+1,l)];
                                                   ped_av_n2_y=[posy(i,l) posy(i+1,l)];
                                                   [x]=polyxpoly(ped_av_n_x,ped_av_n_y,ped_av_n2_x,ped_av_n2_y);

                                               else 

                                                   ped_av_n_x=[nodes(i-1,4,m,i+1) nodes(i+1,4,m,i+1)];
                                                   ped_av_n_y=[nodes(i-1,5,m,i+1) nodes(i+1,5,m,i+1)];
                                                   ped_av_n2_x=[posx(i-1,l) posx(i+1,l)];
                                                   ped_av_n2_y=[posy(i-1,l) posy(i+1,l)];
                                                   [x]=polyxpoly(ped_av_n_x,ped_av_n_y,ped_av_n2_x,ped_av_n2_y);

                                               end
                                               
                                                    if isempty(x)==0

                                                        intersection_control(l,1)=x(1);

                                                        break

                                                     end
                                           
                                           
                                                                              
                                        else

                                           d(n,1)=eps+10;

                                        end

                                    end
                                    
                                else 
                                    
                                    d=eps+10*ones(dim,1);
                                    
                                end
                                 
                                     if obst==0 & d>eps*ones(dim,1) & intersection_control==zeros(dim,1)
                                         
                                        %1:constraint respected
                                        
                                            nodes(i+1,2,m,i+1)=1;
                                            
                                        
                                 
                                        %% calculate the minimum distance to the obstacle only for those generations that encounter the obstacle as the first estimate
                                          if generazioni_da_considerare(1)==i || generazioni_da_considerare(2)==i || generazioni_da_considerare(3)==i
                                           
                                                [val]=calcolo_min_dist_obstacle(controllo(1),controllo(2),incrocio_ostacolo,Ostacolo_x,Ostacolo_y);
                                                ro=7500;
                                                
                                               [costo]=cost_computation(targetx_n,targety_n,nodes,m,i,val,ro);
                                               
                                          else 
                                                
                                                ro=1;
                                                val=50;
                                                [costo]=cost_computation(targetx_n,targety_n,nodes,m,i,val,ro);
                                                
                                         end
                                            nodes(i+1,3,m,i+1)=costo;

                                    end
                        else
                        
                            salvo(:,:,z,1)=zeros(riga_shrinking,5,1,1);
                            nodes(:,:,m,i+1)=salvo(:,:,z,1);
                                
                        end
                        
                    m=m+1;
                    
                end
                
                
        end
        
        
end
        
        
        
        
        
       
            
return         











