function  [strategie_finali_k,posizioni_finali_x,posizioni_finali_y,value,nodes]=opt_with_obstacle(riga_shrinking,Robot_x_target,Robot_y_target,v,angle,T,xStart,yStart,n,scelte,strategie_somma,deltat,generazione,X,Y,MAP,posizioni_finali_x,posizioni_finali_y,strategie_finali_k,Ostacolo_x,Ostacolo_y,incrocio_ostacolo,generazioni_da_considerare,starx,stary,giocatori,eps)

    if giocatori~=1

        [posx,posy]=calcolo_posizioni_altro_giocatore(xStart,yStart,strategie_finali_k,n,deltat,v,T,giocatori);

    else

        posx=zeros(T+1,giocatori);
        posy=zeros(T+1,giocatori);

    end
    
        
        if n==giocatori

            targetx_n=Robot_x_target;
            targety_n=Robot_y_target;

        else

              targetx_n=starx(n);
              targety_n=stary(n);

        end

                                                     
                            [nodes]=tree_generation_obstacle(riga_shrinking,eps,xStart,yStart,angle,n,T,scelte,strategie_somma,deltat,v,generazione,targetx_n,targety_n,X,Y,MAP,Ostacolo_x,Ostacolo_y,incrocio_ostacolo,generazioni_da_considerare,posx,posy,giocatori);
                             
                              
                              %% computes strategy with the minumum cost

                                [idx,value]=best_strategy(nodes,riga_shrinking,scelte);
                                
                                %% update strategy
                                
                                strategie_finali_k(:,n)=zeros(T+1,1);
                                strategie_finali_k(1:riga_shrinking,n)=nodes(1:riga_shrinking,1,idx,riga_shrinking);
                                                            
                                posizioni_finali_x(:,n)=zeros(T+1,1);
                                posizioni_finali_y(:,n)=zeros(T+1,1);
                                
                                posizioni_finali_x(1:riga_shrinking,n)=nodes(1:riga_shrinking,4,idx,riga_shrinking);
                                posizioni_finali_y(1:riga_shrinking,n)=nodes(1:riga_shrinking,5,idx,riga_shrinking);
                                
return