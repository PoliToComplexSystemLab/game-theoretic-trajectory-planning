%% First estimation for all players and plots first figure

function [gioc_x,gioc_y,riga_shrinking]=prime_stime(p,giocatori,xStart,yStart,T,v,angle,deltat,BW,Robot_x_target,Robot_y_target,starx,stary,angle_save)

riga_shrinking=T+1;

    for i=1:giocatori
        
            pos_temp_nx=xStart(i);
            pos_temp_ny=yStart(i);
            
            pos_temp_nx2(1,i)=xStart(i);
            pos_temp_ny2(1,i)=yStart(i);
            
            gioc_x(1,i) = xStart(i);
            gioc_y(1,i) = yStart(i);
            
            
            if i==giocatori
                
                radianti=atan2(Robot_y_target-yStart(giocatori),Robot_x_target-xStart(giocatori));
                decimali=rad2deg(radianti);
                angle_save(p,giocatori)=floor(decimali);
                p1=[pos_temp_nx pos_temp_ny];
                p2=[Robot_x_target Robot_y_target];
                d1=distanza(p1,p2);
                
            end

            for j=1:T

                pos_temp_nx=pos_temp_nx+v*cosd(angle_save(p,i))*deltat;
                pos_temp_ny=pos_temp_ny+v*sind(angle_save(p,i))*deltat;
                               
                pos_temp_nx2(j+1,i)=pos_temp_nx2(j,i)+v*cosd(angle(i))*deltat;
                pos_temp_ny2(j+1,i)=pos_temp_ny2(j,i)+v*sind(angle(i))*deltat;
                
                    if i==giocatori 
                        
                        q1=[pos_temp_nx pos_temp_ny];
                        q2=[Robot_x_target Robot_y_target];
                        [distanza_target]=distanza(q1,q2);
                        
                        if (distanza_target<=90 | d1<=90) & j<=T-1
                            
                             riga_shrinking=j+1;
                             gioc_x(j+1,i)=pos_temp_nx;
                             gioc_y(j+1,i)=pos_temp_ny;                            
                             gioc_x(riga_shrinking+1:(T+1),giocatori)=zeros((T+1)-(riga_shrinking+1)+1,1);
                             gioc_y(riga_shrinking+1:(T+1),giocatori)=zeros((T+1)-(riga_shrinking+1)+1,1);
                              
                            break
                            
                        else
                        
                            gioc_x(j+1,i)=pos_temp_nx;
                            gioc_y(j+1,i)=pos_temp_ny;

                        end
                        
                    else
                        
                        gioc_x(j+1,i)=pos_temp_nx;
                        gioc_y(j+1,i)=pos_temp_ny;
                        
                    end

            end
    end
    
%% Plot figura 1: First estimation for all players

figure(1)
clf
title('First estimation')
imshow(BW);
hold on
axis on
hold on

for i=1:giocatori
    
    if i~=giocatori
        
        plot(gioc_x(:,i),gioc_y(:,i))
        hold on 
        plot(gioc_x(:,i),gioc_y(:,i),'or')
        hold on 
        plot(gioc_x(1,i),gioc_y(1,i),'xr');
        text(gioc_x(1,i)+.5,gioc_y(1,i)+.5,['Start',num2str(i)]);
        hold on 
        plot(starx(i),stary(i),'xr');
        text(starx(i)+.5,stary(i)+.5,['Target',num2str(i)]);
        
    else
        
        plot(gioc_x(1:riga_shrinking,i),gioc_y(1:riga_shrinking,i))
        hold on 
        plot(gioc_x(1:riga_shrinking,i),gioc_y(1:riga_shrinking,i),'or')
        hold on
        plot(xStart(1,i),yStart(1,i),'xr');
        text(xStart(1,i)-.5,yStart(1,i)-.5,['Start robot']);
        hold on  
        plot(Robot_x_target,Robot_y_target,'xr');
        text(Robot_x_target+.5,Robot_y_target+.5,['Target robot']);
        
    end
    
end    

return
