 %cont1=instants of time
 %cont2=players
 % i == numbers of frame
 % pos_x e pos_y: position of players inside the video 
 
 function [xStart,yStart,giocatori,angle]=dati(i,Zara,crowdszara01,max_frame)
 
cont1=1;
cont2=1;

%for i=0:20:max_frame
    
    %legge i frame direttamente dal video 
                 
   if i>=0 & i<=15
       
       [riga]=find(crowdszara01(:,3)>=i & crowdszara01(:,3)<=i+10 )
       
   else
       
       [riga]=find(crowdszara01(:,3)>=i-23 & crowdszara01(:,3)<=i+23)
       
   end
   
   salva=size(riga,1);
   
   for j=1:salva
       
       if j~=1 & riga(j)==riga(j-1)+1
           
           non_fare_niente=1;
           
       else
           
          value=riga(j); 
          pos_x(cont1,cont2)=crowdszara01(value,1);
          pos_y(cont1,cont2)=crowdszara01(value,2);
          direction(cont1,cont2)=crowdszara01(value,4);
          
          
          for k=value:max_frame
              
              if crowdszara01(k+1,3)~=-1  % vuol dire che c'� un nuovo pedone nel frame in questione 
                  
                  cont1=cont1+1;
                  pos_x(cont1,cont2)=crowdszara01(k+1,1);
                  pos_y(cont1,cont2)=crowdszara01(k+1,2);
                  
              else
                  
                 xStart(1,cont2)=pos_x(1,cont2);
                 yStart(1,cont2)= pos_y(1,cont2);
                 angle(1,cont2)=direction(1,cont2);
                 
                 hold on 
                 
            
                 if cont1~=1
                 
                     %obiettivo:trovare il punto di inizio considerando
                     %un'interpolazione lineare.
                     
                     p1=[pos_x(1,cont2) pos_y(1,cont2)];
                     p2=[pos_x(2,cont2) pos_y(2,cont2)];
                     d3=distanza(p1,p2);
                     velocity= d3 /(crowdszara01(value+1,3)-crowdszara01(value,3));
                     x=pos_x(1,cont2)+velocity*cosd(crowdszara01(value,4))*(i-crowdszara01(value,3));
                     y=pos_y(1,cont2)+velocity*sind(crowdszara01(value,4))*(i-crowdszara01(value,3));
                     
                     
                 
                     pos_x(1,cont2)=x;
                     pos_y(1,cont2)=y;
                   
                 
                 end
                 
             xStart(1,cont2)= pos_x(1,cont2);
             yStart(1,cont2)= pos_y(1,cont2);
             
                 cont2=cont2+1;
                 cont1=1;
                 %pause 
                 
                break
                
              end
              
          end
           
       end
       
    end
   
  giocatori=cont2-1; 
  
   %riaggiorno le posizioni iniziali
   
    
   
   
% pause
 
%end

return

