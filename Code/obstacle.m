
%% output:
%incrocio_ostacolo = indicates the i-th obstacle that is hit during the
%first estimate
%obst = 0-->n-th player does not meet any obstacle 
%obst = 1--> otherwise
%generazioni_da_considerare = Given the first estimates it is possible to understand which
%are the generations in which it is necessary to consider the distance
%from the obstacle

function [obst,incrocio_ostacolo,generazioni_da_considerare]=obstacle(x_n,y_n,MAP,X,Y,Ostacolo_x,Ostacolo_y,pos_nx,pos_ny,T)

dim=size(x_n,1);
 
obst=0;
exit=0;
incrocio_ostacolo=[];
generazioni_da_considerare=zeros(1,3);

for  i=1:dim

    x=y_n(i);
    y=x_n(i);

    if x>0 & y>0 & y<X & x<Y
        exit=1;

            if  exit==1 & MAP(x,y)==0 

                    obst=1;
                    break

            end

    end
    
    
end 

  
if obst==1
    
    n_obstacle=size(Ostacolo_x,2);
    

 
    for i=1:n_obstacle

         

    [riga1]=find(y>=Ostacolo_x(1,i) & y<=Ostacolo_x(2,i));
    [riga2]=find(x>=Ostacolo_y(1,i) & x<=Ostacolo_y(2,i));

        if isempty(riga1)==0 & riga1==riga2
            
            incrocio_ostacolo=i;
            
            break

        end

    end
    
  
   
   q1=[y x];
  
    for j=2:T+1
        
        q2=[pos_nx(j) pos_ny(j)];
       dist_gen=distanza(q1,q2); 
       salvo(j-1)=dist_gen;
    
    end
    vet_ordinato=sort(salvo); 
    gen_1=vet_ordinato(1);
    gen_2=vet_ordinato(2);
    gen_3=vet_ordinato(3);
    [generazione1_da_considerare]=find(gen_1==salvo);
    [generazione2_da_considerare]=find(gen_2==salvo);
   [generazione3_da_considerare]=find(gen_3==salvo);
   
   if generazione3_da_considerare <= generazione2_da_considerare 
       
       generazione3_da_considerare=0;
       
   end   
       
generazioni_da_considerare=[generazione1_da_considerare generazione2_da_considerare generazione3_da_considerare];

  
    
end



return