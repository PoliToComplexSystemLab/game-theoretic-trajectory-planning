function [costo]=cost_rall(targetx_n,targety_n,matrice,z,val,ro,i)
% minimization of the distance 

 pstar=[targetx_n targety_n];
 p_t=[matrice(z+1,3,i) matrice(z+1,4,i)];
 dist_target=distanza(pstar,p_t);
 

eps1=[0.6;0.7;0.8;1];


costo=(eps1(z)*dist_target)+ro*(1/(val^2));

return