function [costo]=cost_computation(targetx_n,targety_n,nodes,m,i,val,ro)
% minimization of the distance 

 pstar=[targetx_n targety_n];
 p_t=[nodes(i+1,4,m,i+1) nodes(i+1,5,m,i+1)];
 dist_target=distanza(pstar,p_t);
 
 % smoothness
    for k=1:i
        
        vettore(k)=norm(nodes(k+1,1,m,i+1)-nodes(k,1,m,i+1));
 
    end
    
 somma=sum(vettore); 
    


eps1=[0.6;0.7;0.8;1];

eps2=ones(4,1)-eps1;


costo=(eps1(i)*dist_target)+(eps2(i)*somma)+ro*(1/(val^2));

return