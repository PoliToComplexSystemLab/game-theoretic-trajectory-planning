%gruppo: contiene i giocatori che sono considerati GRUPPO--> controllo
%prima la direzione e poi la distanza tra i pedoni 
%Ho inserito giocatori-1 perch� il robot � sempre all'ultima posizione e non voglio sia considerato parte del gruppo--> MAI 

function [gruppo]=group(angle,xStart,yStart,giocatori)

gruppo=[];
equal_angle=[];
%eps_gruppo=70;
eps_gruppo=50;
gruppi=[];

%metto che gli angoli disponibili sono tutti tranne quello del robot

angle=angle(1:giocatori-1);

%controllo della direzione con tolleranza--> non considerando gi� a priori
%il robot come appartenente al gruppo--> per questo motivo i va da 1 a
%giocatori -1

for i=1:giocatori-1
    
    % gli altri 2 o sono inseriti per catalogare come gruppo anche quei
    % pedoni che hanno +180 e -180;
    
    %[rows,column]=find(angle>=angle(i)-10 & angle<=angle(i)+10 | (angle-angle(i)>=-360 & angle-angle(i)<=-350) | (angle-angle(i)>=350 & angle-angle(i)<=360));
    [rows,column]=find(angle>=angle(i)-6 & angle<=angle(i)+6 | (angle-angle(i)>=-360 & angle-angle(i)<=-354) | (angle-angle(i)>=354 & angle-angle(i)<=360));
   % [rows,column]=find(angle>=angle(i)-5 & angle<=angle(i)+5 | (angle-angle(i)>=-360 & angle-angle(i)<=-355) | (angle-angle(i)>=355 & angle-angle(i)<=360));

    equal_angle(i,i)=i;
    equal_angle(column,i)=column;
        
end


for i=1:giocatori-1 %con giocatori-1 non considero la colonna relativa al gruppo del robot
    
    confronto=equal_angle(:,i);
    
    for j=1:giocatori-1
        
        C=zeros(giocatori-1,1);
        
        if i~=j
            
             [riga]=find(confronto(:)==equal_angle(:,j));
             dim_riga=size(riga,1);
             
             for z=1:dim_riga 
                 
                riga_C=riga(z);
               
                    C(riga_C,1)=confronto(riga_C,1);
               
             end
               % se la C � tutta zero vuole dire che non esististono variabili COMUNI tra i due vettori          
                if C==zeros(giocatori-1,1)
                    C=[];
                end
                
                 dim_gruppi=size(gruppi,2);
                 
                 if isempty(C)==0 & dim_gruppi~=0
                     
                     % D serve per trovare le variabili non uguali
                     D=zeros(giocatori-1,1);
                     F=abs(confronto-equal_angle(:,j));     
                     %dim_F=size(F,1);
                       %D(1:dim_F)=F; 
                       D=F;
                       gruppo_1=C+D;
                       dim_c_save=[];
                       for k=1:dim_gruppi
                           
                              controllo = intersect(gruppo_1,gruppi(:,k));
                              dim_ver=size(controllo,1);                             
                              [trovata_riga]=find(zeros(1,1)==controllo);
                              dim_c_save(k)=dim_ver;
                              if dim_ver>=2
                                  [contatore_gruppi]=find(zeros(1,1)==gruppi(:,k));
                                  [contatore_gruppo_1]=find(zeros(1,1)==gruppo_1);
                                 s_contatore_gruppi=size(contatore_gruppi,1);
                                 s_contatore_gruppo_1=size(contatore_gruppo_1,1);
                                  break
                              end
                         
                       end
 
                       dim_c=max(dim_c_save);
                        if (dim_c>1) | (dim_c==1 & isempty(trovata_riga)==1)
                            
                            exit=1;
                            %non bisogna aggiungere il vettore nella
                            %matrice
                             
    % pezzo aggiunto perch� se ho group=[1 2 0 0] e gruppo1=[1 2 3 0] gruppo1 viene scartato! 
                                if dim_c>=2 & s_contatore_gruppi>s_contatore_gruppo_1

                                    gruppi(:,k)=gruppo_1(:);

                                end
                            
                            else  
                               gruppi(:,dim_gruppi+1)=gruppo_1(:);
                           
                        end 
                 end
           
             if isempty(C)==0 & dim_gruppi==0
                 
                 %da controllare--> � qui il problema del plot 
                 
                 %D=zeros(giocatori,1);
                 F=abs(confronto-equal_angle(:,j));     
                 %dim_F=size(F,1);
                 %D(1:dim_F)=F; 
                 D=F;
                 gruppo_1=C+D;
                
                 
                 %D=abs(confronto-equal_angle(:,j));
                 %gruppo_1=C+D;
%                 [riga_prova]=find(giocatori==gruppo_1)
%               
%                 if isempty(riga_prova)==1
%                     
%                      gruppi(:,dim_gruppi+1)=gruppo_1(:);
%                      
%                 else
%                     
%                     % in questo modo non considero il robot nel gruppo                    
%                     gruppo_1(riga_prova)=0;

                    gruppi(:,dim_gruppi+1)=gruppo_1(:);
                    
                %end
                
            end
                 
        end
         
    end
    
 end


%% calcolo delle distanze
  
if isempty(gruppi)==0
       
    dim1=size(gruppi,1);
    dim2=size(gruppi,2);
    gruppo=zeros(giocatori-1,dim2);
    
    for i=1:dim2 % selezioni la colonna 
    
        for j=1:dim1-1 %selezioni la riga (
            
            for k=j+1:dim1
                
               g1=gruppi(j,i);
               g2=gruppi(k,i);
               
               if g1~=0 & g2~=0 & g1~=g2
                   
                   q1=[xStart(g1) yStart(g1)];
                   q2=[xStart(g2) yStart(g2)];
                   d = distanza(q1,q2);
                   
                   if d<eps_gruppo
                       
                        gruppo(j,i)=j;
                        gruppo(k,i)=k;
                        
                   end
                   
               end
              
            end
            
        end
         
    end
end

return
