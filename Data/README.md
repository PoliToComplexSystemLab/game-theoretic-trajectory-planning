**DATA READING GUIDE**

The collected and anonymized aggregated data can be found in the file "Anonymous_aggregated_data" in .xls format. The file contains self-explanatory labels

The data sheet presents all data collected during the experiment used for the results in the paper "Game-theoretic trajectory planning enhances social acceptability for humans".

